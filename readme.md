# The Movie Database Downloader

This project is built in order to download the entire dataset of www.themoviedb.org.

In order to do that, it uses the `tmdbsimple` python module made by Celia Oakley, available at `pypi` (https://pypi.python.org/pypi/tmdbsimple)

This module downloads metadata, cast, and crew information by calling three times to the API for each item id.

## Setting up

There is a `requirements.txt` file in this repository.

You can easily install the dependencies like this:

```bash
pip install -r requirements.txt
```


## Launching

These are the parameters for launching the `tmdb_downloader.py` module, you can see the same by typing `tmdb_downloader.py --help`:

```bash
usage: tmdb_downloader.py [-h] --download_type {movies,tv_shows}
                          [--from_id FROM_ID] [--to_id TO_ID]
                          [--output_path OUTPUT_PATH]
                          [--ids_file_path IDS_FILE_PATH]

The Movie Database downloader

optional arguments:
  -h, --help            show this help message and exit
  --download_type {movies,tv_shows}
                        Download type: movies or tv_shows. Required.
  --from_id FROM_ID     Beginning movie/tv shwow id. First id by default.
  --to_id TO_ID         Beginning movie/tv shwow id. Latest by default.
  --output_path OUTPUT_PATH
                        Output path to store the dataset. Default value in
                        settings.
  --ids_file_path IDS_FILE_PATH
                        Path to plain file containing a list of ids to
                        download, one per line. When this parameter is
                        specified, 'from_id' and 'to_id' parameters are
                        ignored.
```

The only mandatory parameter is `download_type`. If no other parameter passed, it will download the entire dataset.

You can specify a range of ids using `from_id` and `to_id` parameters.

If you choose the last option `ìds_file_path`, you can select your own set of items to download by wrinting a list of ids to a plain text file, one item id per line. In this case, `from-id` and `to_id` are ignored.