import argparse
import importlib.machinery
import logging
from datetime import datetime

import types
from controller.tmdb_controller import TMDBController


class TheMovieDBDownloader:
    def __init__(self, settings_path):
        loader = importlib.machinery.SourceFileLoader(settings_path, settings_path)
        self.__settings = types.ModuleType(loader.name)
        loader.exec_module(self.__settings)
        self.__tmdb_controller = TMDBController(self.__settings.API_KEY)

    def run_downloader(self, download_type, from_id, to_id, output_path, ids_file_path):
        try:
            logging.info("Running The Movie Database downloader...")

            if not output_path:
                output_path = self.__settings.OUTPUT_PATH

            if not ids_file_path:

                if not from_id:
                    from_id = self.__settings.DEFAULT_FROM

                if download_type == self.__settings.MOVIES_TYPE:
                    if not to_id:
                        to_id = self.__tmdb_controller.get_latest_movie_id()
                    logging.info("Parameters: download type: {}, from id: {}, to id: {}".format(download_type, from_id, to_id))
                    self.__tmdb_controller.store_movies(from_id, to_id, output_path, logging)
                else:
                    if not to_id:
                        to_id = self.__tmdb_controller.get_latest_tv_show_id()
                    logging.info("Parameters: download type: {}, from id: {}, to id: {}".format(download_type, from_id, to_id))
                    self.__tmdb_controller.store_tv_shows(from_id, to_id, output_path, logging)
            else:
                list_ids = TheMovieDBDownloader.read_lines_from_file(ids_file_path)
                if download_type == self.__settings.MOVIES_TYPE:
                    logging.info("Parameters: download type: {}, ids_file_path: {}".format(download_type, ids_file_path))
                    self.__tmdb_controller.store_movies(output_path, list_ids, logging)
                else:
                    logging.info("Parameters: download type: {}, ids_file_path: {}".format(download_type, ids_file_path))
                    self.__tmdb_controller.store_tv_shows(output_path, list_ids, logging)

        except Exception as ex:
            logging.exception("[The Movie Database downloader] Exception: {}".format(ex.args))
            exit(-1)

    def read_lines_from_file(file_path):
        return [line.rstrip('\n') for line in open(file_path)]



def main():
    loader = importlib.machinery.SourceFileLoader("config/settings.py", "config/settings.py")
    settings = types.ModuleType(loader.name)
    loader.exec_module(settings)
    logging.basicConfig(filename=settings.LOG_PATH + str(datetime.today()), level=logging.INFO, format=settings.LOG_FORMAT)

    parser = argparse.ArgumentParser(description='The Movie Database downloader')
    parser.add_argument('--download_type', choices=[settings.MOVIES_TYPE, settings.TV_SHOWS_TYPE],
                        help='Download type: movies or tv_shows. Required.', required=True)
    parser.add_argument('--from_id', type=valid_id, help='Beginning movie/tv shwow id. First id by default.', required=False)
    parser.add_argument('--to_id', type=valid_id, help='Beginning movie/tv shwow id. Latest by default.', required=False)
    parser.add_argument('--output_path', help='Output path to store the dataset. Default value in settings.', required=False)
    parser.add_argument('--ids_file_path', help='Path to plain file containing a list of ids to download, one per line.'
                                                ' When this parameter is specified, \'from_id\' and \'to_id\' '
                                                'parameters are ignored.', required=False)

    args = parser.parse_args()
    logging.info("Params <{}>".format(args))
    logging.info("Starting download process from TMDB")
    try:
        tmdb_downloader = TheMovieDBDownloader("config/settings.py")
        tmdb_downloader.run_downloader(args.download_type, args.from_id, args.to_id, args.output_path, args.ids_file_path)
    except Exception as ex_main:
        logging.error("ERROR: An error raised and the process ended: {}".format(str(ex_main)))
        exit(-1)

    logging.info("Download process finished!")


def valid_id(i):
    try:
        val = int(i)
        if val > 0:
            return val
        else:
            msg = "Not a valid integer: '{0}'. Value must be a positive integer.".format(i)
            raise argparse.ArgumentTypeError(msg)
    except ValueError:
        msg = "Not a valid integer: '{0}'. Value must be a positive integer.".format(i)
        raise argparse.ArgumentTypeError(msg)


if __name__ == '__main__':
    main()
