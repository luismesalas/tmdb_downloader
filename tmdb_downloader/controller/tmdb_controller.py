import json

import tmdbsimple as tmdb


class TMDBController(object):
    def __init__(self, api_key):
        self.__tmdb = tmdb
        self.__tmdb.API_KEY = api_key
        self.__movies_repository = self.__tmdb.Movies()
        self.__tv_repository = self.__tmdb.TV()

    def get_latest_movie_id(self):
        return self.__movies_repository.latest()['id']

    def get_latest_tv_show_id(self):
        return self.__tv_repository.latest()['id']

    def store_movies(self, from_id, to_id, path, logging):
        cursor = from_id
        while cursor <= to_id:
            self.store_movie(cursor, logging, path)
            cursor += 1

    def store_movies(self, path, list_ids, logging):
        for id in list_ids:
            self.store_movie(id, logging, path)

    def store_movie(self, movie_id, logging, path):
        logging.info("[The Movie Database downloader] Retrieving movie with id '{}'.".format(movie_id))
        movie = self.__tmdb.Movies(movie_id)
        try:
            info = movie.info()
            if info:
                try:
                    credits = movie.credits()
                    if credits:
                        info.update({'cast': credits['cast']})
                        info.update({'crew': credits['crew']})
                except Exception:
                    logging.exception(
                        "[The Movie Database downloader] Cannot get cretdits info for movie with id '{}'.".format(
                            movie_id))

                with open('{}/movie_{}.json'.format(path, movie_id), 'w') as outfile:
                    json.dump(info, outfile, ensure_ascii=False)

        except Exception:
            logging.exception("[The Movie Database downloader] Movie with id '{}' not found.".format(movie_id))

    def store_tv_shows(self, from_id, to_id, path, logging):
        cursor = from_id
        while cursor <= to_id:
            self.store_tv_show(cursor, logging, path)
            cursor += 1

    def store_tv_shows(self, path, list_ids, logging):
        for id in list_ids:
            self.store_tv_show(id, logging, path)

    def store_tv_show(self, tv_show_id, logging, path):
        logging.info("[The Movie Database downloader] Retrieving TV show with id '{}'.".format(tv_show_id))
        tv_show = self.__tmdb.TV(tv_show_id)
        try:
            info = tv_show.info()
            if info:
                try:
                    credits = tv_show.credits()
                    if credits:
                        info.update({'cast': credits['cast']})
                        info.update({'crew': credits['crew']})
                except Exception:
                    logging.exception(
                        "[The Movie Database downloader] Cannot get cretdits info for TV show with id '{}'.".format(
                            tv_show_id))

                with open('{}/tv_{}.json'.format(path, tv_show_id), 'w') as outfile:
                    json.dump(info, outfile, ensure_ascii=False)
        except Exception:
            logging.exception("[The Movie Database downloader] TV show with id '{}' not found.".format(tv_show_id))
